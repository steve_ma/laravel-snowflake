# laravel-snowflake

#### 介绍
生成唯一的id-根据雪花算法自己DIY了一下

### 安装教程

```shell

# 安装

$ composer require stevema/laravel-snowflake

```


### 使用说明

```php
# 1、生成配置文件
$ php artisan vendor:publish --tag="snowflake"

# 2、修改配置文件 /config/snowflake.php 或在 /.env 文件中添加配置

# 数据中心位数  默认 5字节
SNOW_DATA_CENTER_LEN=5
# 二进制后不够会自动补位 5字节 的情况下 0-31
SNOW_DATA_CENTER_ID=0
# 数据节点位数  默认 5字节
SNOW_WORKER_LEN=5
# 二进制后不够会自动补位 5字节 的情况下 0-31
SNOW_WORKER_ID=1
# 时间戳格式  microtime(42) time(秒 10位int) minute(分钟 8位int)
# microtime(毫秒) 4398046511(000) => 2109-5-15 15:35:11 42位的时间戳最多可以用到这个时候
# time(秒) 2147483647 => 2038-01-19 11:14:07 31位的二进制最多可以用到这个时候
# time(秒) 4294967295 => 2106-02-07 14:28:15 32位的二进制最多可以用到这个时候
# minute(分) 67108863(fen) -> 4026531839(秒) => 2097-08-05 17:03:59 26位的二进制最多可以用到这个时候
# 一般来说 业务是到不了毫秒级的并发的 用秒就够了 位数填 32位够够的了 但是默认一定要规范 microtime 42
SNOW_TIME_TYPE=microtime
# 时间戳二进制的字节数 默认 42
SNOW_TIME_LEN=42
# 序列数类型  rand 根据字节数随机  cache  根据默认缓存自增
SNOW_INCR_TYPE=rand
# 序列数二进制的字节数 默认 12
SNOW_INCR_LEN=12
# 美化输出的字符串 bool  false => MP32-S91H97B9VPA true => MP32-0S91-H97B-9VPA
SNOW_BEAUTIFY_CODE=true
# 美化后输出的字符串分割长度 4 => MP32-0S91-H97B-9VPA 5 => MP32-00A3J-TWC2Y
SNOW_BEAUTIFY_LEN=5
```

### 目录说明

```
├─ src
│   ├─ Config               # 配置文件目录
│   │   └─ config.php       # 配置文件
│   ├─ Facades              # 门面文件目录
│   │   └─ SnowFlake.php    # 门面
│   └─ helper.php           # 帮助文件
│   └─ SnowFlakeException.php   # 异常文件
│   └─ SnowFlakeManage.php  # 项目主文件
│   └─ SnowFlakeProvider.php    # 服务者
└─ composer.json            # 配置

```

### 使用方式

```php
# 引用门面
use Stevema\Facades\SnowFlake;

# 随便加一个路由
Route::get('/t/afs', function(){
    
    # 第一种方式 来自门面 Stevema\Afs\Facades\Afs 上面use的
    # map32 是配置中的map 可以自己随意配置 通过进制算法把10进制的 $intId 转换成 $strId
//    $intId = SnowFlake::createId();
//    $strId = SnowFlake::encode($intId, 'map32');
//    $intId = SnowFlake::decode($strId);
    
    # 如果不引入 这样用
//    $token = \Stevema\Facades\SnowFlake::createId();
    
    # 第二种方式 来自 app()
//    $snow = app('snowflake');
//    $intId = $snow->createId();
//    $strId = $snow->encode($intId, 'map32');
//    $intId = $snow->decode($strId);
    
//    $strId = $snow->createId('map32'); // 效果等同于上面
//    $intId = $snow->decode($strId);
    
    
    # 第三种方式  来自 helper 
//    $snow = snowflake();
//    $intId = $snow->createId();
//    $strId = $snow->encode($intId, 'map32');
//    $intId = $snow->decode($strId);

//    $intId = snowflake_create_id();
//    $strId = snowflake_encode($intId, 'map32')
//    $strId = snowflake_create_id('map32');
//    $intId = snowflake_decode($strId);
    
    # 如果你想临时修改配置
    $snow = snowflake();
    // 数据中心位数  默认 5字节
    $snow->setDataCenterLen(5);
    // 5字节 的情况下 0 - 31 默认 0
    $snow->setDataCenterId(0);
    // 数据节点位数  默认 5字节
    $snow->setWorkerLen(5);
    // 5字节 的情况下 0 - 31 默认 0
    $snow->setWorkerId(0);
    // 时间戳格式  microtime(毫秒 13位int) time(秒 10位int) minute(分钟 8位int)
    $snow->setTimeType('time');
    // microtime(毫秒) 4398046511(000) => 2109-5-15 15:35:11 42位的时间戳最多可以用到这个时候
    // time(秒) 2147483647 => 2038-01-19 11:14:07 31位的二进制最多可以用到这个时候
    // time(秒) 4294967295 => 2106-02-07 14:28:15 32位的二进制最多可以用到这个时候
    // minute(分) 67108863(fen) -> 4026531839(秒) => 2097-08-05 17:03:59 26位的二进制最多可以用到这个时候
    // 一般来说 业务是到不了毫秒级的并发的 用秒就够了 位数填 32位够够的了 但是默认一定要规范 hm 42
    $snow->setTimeLen(32);
    // 序列数类型  rand 根据字节数随机  cache  根据默认缓存自增
    $snow->setIncrType('rand');
    // 序列数 12字节 0-4095  10字节 0-1023
    $snow->setIncrLen(12);
    
    // maps也可以设置
    $snow->setMaps([
        'mymap' => [
            'prefix' => 'ABCD',
            'map' => ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','J','K','M','N','P','Q','R','S','T','U','V','W','Y','Z'],
        ],      
    ]);
    // 想看一下调试信息 - 
    $snow->setDebug(true);
   
    $strId = $snow->createId('mymap'); // 效果等同于上面
    $intId = $snow->decode($strId);
    
    dd([
        'intId' => $intId, // 457643568423 
        'strId' => $strId, // ABCD-DA6TWB97
    ]);
    
//    "timestamp_decbin:01101011101010110101100110[26]" // packages/laravel-snowflake/src/SnowFlakeManage.php:266
//    "dataCenter_decbin:00000[5]" // packages/laravel-snowflake/src/SnowFlakeManage.php:269
//    "woker_decbin:00001[5]" // packages/laravel-snowflake/src/SnowFlakeManage.php:272
//    "incr_type:rand" // packages/laravel-snowflake/src/SnowFlakeManage.php:238
//    "incr_decbin:101111010100[12]" // packages/laravel-snowflake/src/SnowFlakeManage.php:275
//    "decbin_id:000000000110111101010001101011101010110101100110[48]" // packages/laravel-snowflake/src/SnowFlakeManage.php:289
//    "int_id:478111772006" // packages/laravel-snowflake/src/SnowFlakeManage.php:292
//    "map_name:map32" // packages/laravel-snowflake/src/SnowFlakeManage.php:311
//    "map_prefix:MP32" // packages/laravel-snowflake/src/SnowFlakeManage.php:317
//    "map_map:["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","J","K","M","N","P","Q","R","S","T","U","V","W","Y","Z"]" // packages/laravel-snowflake/src/SnowFlakeManage.php:318
//    "map_strId:00A3JTWC2Y" // packages/laravel-snowflake/src/SnowFlakeManage.php:381
//    "map_map:["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","J","K","M","N","P","Q","R","S","T","U","V","W","Y","Z"]" // packages/laravel-snowflake/src/SnowFlakeManage.php:382
//    array:2 [▼ // routes/api.php:91
//      "intId" => 423619375205
//      "strId" => "MP32-CAGT-WC35"
//    ]
    
});

```

### 备注

1. 单项目的情况下  数据中心 和 数据节点可以把位数设置成0
2. 多个项目可以调整 数据节点 的字节数来增加节点数 
3. 二进制默认是 数据中心(默认5字节) 数据节点(默认5字节) 序列数(默认12字节) 时间戳(默认42字节) 自己酌情调整
4. 接下来就要写traits了 model直接use后可以直接赋值
