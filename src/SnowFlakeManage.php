<?php
namespace Stevema\SnowFlake;
use Illuminate\Support\Facades\Cache;
/**
 *  DIY雪花算法生成分布式id
 */
class SnowFlakeManage {
    // 数据中心位数  默认 5字节  二进制后不够会自动补位 5字节 的情况下 0 - 31 默认 0
    protected int $dataCenterLen = 5;
    protected int $dataCenterId = 0;
    // 数据节点位数  默认 5字节  二进制后不够会自动补位 5字节 的情况下 0 - 31 默认 0
    protected int $workerLen = 5;
    protected int $workerId = 0;

    // 时间戳格式  microtime(毫秒 13位int) time(秒 10位int) minute(分钟 8位int) -> 二进制 microtime(42位) time(31位) minute(26位)
    // microtime(毫秒) 4398046511(000) => 2109-5-15 15:35:11 42位的时间戳最多可以用到这个时候
    // time(秒) 2147483647 => 2038-01-19 11:14:07 31位的二进制最多可以用到这个时候
    // time(秒) 4294967295 => 2106-02-07 14:28:15 32位的二进制最多可以用到这个时候
    // minute(分) 67108863(fen) -> 4026531839(秒) => 2097-08-05 17:03:59 26位的二进制最多可以用到这个时候
    // 一般来说 业务是到不了毫秒级的并发的 用秒就够了 位数填 32位够够的了 但是默认一定要规范 hm 42
    protected string $timeType = 'microtime';
    protected array $timeTypes = ['microtime','time','minute'];
    protected int $timeLen = 42;

    // 序列数类型  rand 根据字节数随机  cache  根据默认缓存自增
    protected string $incrType = 'rand';
    protected array $incrTypes = ['rand','cache'];
    // 序列数 12字节 0-4095  10字节 0-1023
    protected int $incrLen = 12;
    // 美化输出的字符串
    protected bool $beautifyCode = true;
    // 美化后输出的字符串分割长度
    protected int $beautifyLen = 4;
    // 打印相关信息
    protected bool $debug = false;

    // 默认 map
    protected array $maps = [
        'map32' => [
            'prefix' => 'MP32',
            // 默认 32位字典 10位数字和26位字母 去掉容易混淆的 O I L X 后剩下的 32位 你也可以自己把位置换了
            'map' => ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','J','K','M','N','P','Q','R','S','T','U','V','W','Y','Z'],
        ]
    ];
    public function __construct(Array $config){
        $this->setDataCenterLen(intval($config['snow_data_center_len']));
        $this->setDataCenterId(intval($config['snow_data_center_id']));
        $this->setWorkerLen(intval($config['snow_worker_len']));
        $this->setWorkerId(intval($config['snow_worker_id']));
        $this->setTimeType(strval($config['snow_time_type']));
        $this->setTimeLen(intval($config['snow_time_len']));
        $this->setTimeType(strval($config['snow_time_type']));
        $this->setTimeLen(intval($config['snow_time_len']));
        $this->setIncrType(strval($config['snow_incr_type']));
        $this->setIncrLen(intval($config['snow_incr_len']));
        $this->setBeautifyCode(boolval($config['snow_beautify_code']));
        $this->setBEautifyLen(intval($config['snow_beautify_len']));
        $this->setMaps($config['snow_maps']);


    }

    /**
     * @param Int $len
     * @return void
     */
    public function setDataCenterLen(Int $len): void
    {
        $this->dataCenterLen = $len;
    }

    /**
     * @param Int $id
     * @return void
     * @throws SnowFlakeException
     */
    public function setDataCenterId(Int $id): void
    {
        if($this->dataCenterLen > 0) {
            if ($id < 0 || $id > pow(2, $this->dataCenterLen) - 1) throw new SnowFlakeException("数据中心id的值超出位数限制");
        }
        $this->dataCenterId = $id;
    }

    /**
     * @param Int $len
     * @return void
     */
    public function setWorkerLen(Int $len): void
    {
        $this->workerLen = $len;
    }

    /**
     * @param Int $id
     * @return void
     * @throws SnowFlakeException
     */
    public function setWorkerId(Int $id): void
    {
        if($this->workerLen > 0) {
            if ($id < 0 || $id > pow(2, $this->workerLen) - 1) throw new SnowFlakeException("数据节点id的值超出位数限制");
        }
        $this->workerId = $id;
    }

    /**
     * @param String $type
     * @return void
     * @throws SnowFlakeException
     */
    public function setTimeType(String $type): void
    {
        if(! in_array($type, $this->timeTypes)) throw new SnowFlakeException("时间戳格式 {$type} 不支持 [".implode(",", $this->timeTypes)."]");
        $this->timeType = $type;
    }

    /**
     * @param Int $len
     * @return void
     */
    public function setTimeLen(Int $len): void
    {
        $this->timeLen = $len;
    }

    /**
     * @param String $type
     * @return void
     * @throws SnowFlakeException
     */
    public function setIncrType(String $type): void
    {
        if(! in_array($type, $this->incrTypes)) throw new SnowFlakeException("序列格式 {$type} 不支持 [".implode(",", $this->incrTypes)."]");
        $this->incrType = $type;
    }

    /**
     * @param Int $len
     * @return void
     */
    public function setIncrLen(Int $len): void
    {
        $this->incrLen = $len;
    }

    /**
     * @param bool $beautify
     * @return void
     */
    public function setBeautifyCode(bool $beautify=false): void
    {
        $this->beautifyCode = $beautify;
    }

    /**
     * @param Int $len
     * @return void
     */
    public function setBEautifyLen(Int $len): void
    {
        $this->beautifyLen = $len;
    }

    /**
     * @param array $maps
     * @return void
     */
    public function setMaps(array $maps=[]): void
    {
        if(!empty($maps)) {
            $this->maps = $maps;
        }
    }

    /**
     * @param bool $debug
     * @return void
     */
    public function setDebug(bool $debug=false): void
    {
        $this->debug = $debug;
    }

    /**
     * @return string
     */
    protected function getDecbinTime(): string
    {
        $decbin = '';
        switch($this->timeType){
            case 'microtime';
                $decbin = decbin(floor(microtime(true) * 1000));
                break;
            case 'time';
                $decbin = decbin(time());
                break;
            case 'minute';
                $decbin = decbin(floor(time()/60));
                break;
            default;
                break;
        }
        return str_pad($decbin, $this->timeLen, "0", STR_PAD_LEFT);
    }

    /**
     * @return string
     */
    protected function getDecbinDataCenter():string {
        if($this->dataCenterLen > 0) {
            return str_pad(decbin($this->dataCenterId), $this->dataCenterLen, "0", STR_PAD_LEFT);
        } else {
            return '';
        }
    }

    /**
     * @return string
     */
    protected function getDecbinWorker():string {
        if($this->workerLen > 0) {
            return str_pad(decbin($this->workerId), $this->workerLen, "0", STR_PAD_LEFT);
        } else {
            return '';
        }
    }

    /**
     * @param string $cacheName
     * @return string
     */
    protected function getDecbinIncr(string $cacheName=''): string
    {
        $decbin = '';
        if($this->debug) dump("incr_type:{$this->incrType}");
        switch($this->incrType){
            case 'rand';
                $decbin = decbin(mt_rand(0, pow(2, $this->incrLen) - 1));
                break;
            case 'cache';
                $cache_key = 'snowflake:'.$cacheName;
                if($this->debug) dump("incr_cache_key:{$cache_key}");
                $decbin = decbin(Cache::increment($cache_key, 1));
                break;
            default;
                break;
        }
        if($this->incrLen > 0) {
            return str_pad($decbin, $this->incrLen, "0", STR_PAD_LEFT);
        } else {
            return '';
        }
    }

    /**
     * 获取二进制的id
     * @return string
     */
    protected function createDecbinID(): string
    {
        // 二进制的 时间戳 默认42字节
        $time = $this->getDecbinTime();
        if($this->debug) dump("timestamp_decbin:{$time}[".strlen($time)."]");
        // 二进制的 数据中心 默认5字节
        $dataCenter = $this->getDecbinDataCenter();
        if($this->debug) dump("dataCenter_decbin:{$dataCenter}[".strlen($dataCenter)."]");
        // 二进制的 数据节点 默认5字节
        $worker = $this->getDecbinWorker();
        if($this->debug) dump("woker_decbin:{$worker}[".strlen($worker)."]");
        // 二进制的 序列数 默认12字节
        $incr = $this->getDecbinIncr(md5($dataCenter.$worker.$time));
        if($this->debug) dump("incr_decbin:{$incr}[".strlen($incr)."]");

        return $dataCenter.$worker.$incr.$time;
    }

    /**
     * 生成id
     * @param string $type
     * @return float|int|string
     * @throws SnowFlakeException
     */
    public function createID(string $type='int'): float|int|string
    {
        $decbinId = $this->createDecbinID();
        if($this->debug) dump("decbin_id:{$decbinId}[".strlen($decbinId)."]");

        $intId = bindec($decbinId);
        if($this->debug) dump("int_id:{$intId}");
        if($type == 'int'){
            // 转化为 十进制 返回
            return $intId;
        } else {
            return $this->encode($intId, $type);
        }
    }



    /**
     * 10进制转n进制
     * @param int $intId
     * @param string $mapName
     * @return string
     * @throws SnowFlakeException
     */
    public function encode(int $intId, string $mapName=''): string
    {
        if($this->debug) dump("map_name:{$mapName}");
        if(!isset($this->maps[$mapName])) throw new SnowFlakeException("没找到{$mapName}对应的map");
        $prefix = $this->maps[$mapName]['prefix'];
        $map = $this->maps[$mapName]['map'];
        if(empty($map)) throw new SnowFlakeException("{$mapName}的map是空的");
        if(empty($prefix)) throw new SnowFlakeException("{$mapName}的前缀是空的");
        if($this->debug) dump("map_prefix:{$prefix}");
        if($this->debug) dump("map_map:".json_encode($map));



        $divide = count($map);
        $intId = intval($intId);
        $chars = [];

        while ($intId > 0) {
            $d = $intId % $divide;
            $intId = ($intId - $d) / $divide;
            $chars[] = $map[$d];
        }

        $rel = join('', array_reverse($chars));

        if($this->beautifyCode) {
            return $this->beautifyCode($prefix, $rel, $map[0]);
        }
        return $prefix."-".$rel;
    }

    /**
     * 返回指定格式的 - -
     * @param string $prefix  前缀-指定map
     * @param string $code      编码后的字符串
     * @param string $padString  补全使用的字符串 map[0] 不会影响到decode
     * @return string
     */
    public function beautifyCode(string $prefix, string $code, string $padString='0'){
        // 计算code的长度
        $length = strlen($code);
        // 计算需要补全的长度
        $padlen = $this->beautifyLen-$length%$this->beautifyLen;
        // 补全
        if($padlen < $this->beautifyLen && $padlen > 0) {
            $code = str_pad($code, $length+$padlen, $padString, STR_PAD_LEFT);
        }
        // 按指定长度分割成数组
        $arr = str_split($code, $this->beautifyLen);
        // 最前面插入前缀
        array_unshift($arr, $prefix);
        // 返回字符串
        return implode("-", $arr);
    }

    /**
     * 转成10进制
     * @param string $strId
     * @return float|int
     * @throws SnowFlakeException
     */
    public function decode (string $strId): float|int
    {
        $arr = explode("-", $strId);
        $prefix = $arr[0];
        $strId = str_replace([$prefix, '-'], ["",""], $strId);
        $map = [];
        foreach($this->maps as $v){
            if($v['prefix'] == $prefix){
                $map = $v['map'];
            }
        }
        if($this->debug) dump("map_strId:{$strId}");
        if($this->debug) dump("map_map:".json_encode($map));
        if(empty($map)) throw new SnowFlakeException("没有找到指定的map");
        $c = count($map);
        $len = strlen($strId);
        $sum = 0;
        for($i = 0; $i < $len; $i++) {
            $index = array_search($strId[$i], $map);
            $sum += $index * pow($c, $len - $i - 1);
        }
        return $sum;
    }

}
