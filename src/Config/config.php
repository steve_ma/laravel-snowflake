<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Snowflake Config
    |--------------------------------------------------------------------------
    |
    | 定义自己的雪花算法
    |
    | 数据中心(默认5字节) 数据节点(默认5字节) 序列数(默认12字节) 时间戳(默认42字节)
    |
    |
    */

    // 数据中心位数  默认 5字节  二进制后不够会自动补位 5字节 的情况下 0 - 31 默认 0
    'snow_data_center_len' => env("SNOW_DATA_CENTER_LEN", 5),
    'snow_data_center_id' => env("SNOW_DATA_CENTER_ID", 0),
    // 数据节点位数  默认 5字节  二进制后不够会自动补位 5字节 的情况下 0 - 31 默认 0
    'snow_worker_len' => env("SNOW_WORKER_LEN", 5),
    'snow_worker_id' => env("SNOW_WORKER_ID", 0),
    // 时间戳格式  microtime(毫秒 13位int) time(秒 10位int) minute(分钟 8位int) -> 二进制 microtime(42位) time(31位) minute(26位)
    // microtime(毫秒) 4398046511(000) => 2109-5-15 15:35:11 42位的时间戳最多可以用到这个时候
    // time(秒) 2147483647 => 2038-01-19 11:14:07 31位的二进制最多可以用到这个时候
    // time(秒) 4294967295 => 2106-02-07 14:28:15 32位的二进制最多可以用到这个时候
    // minute(分) 67108863(fen) -> 4026531839(秒) => 2097-08-05 17:03:59 26位的二进制最多可以用到这个时候
    // 一般来说 业务是到不了毫秒级的并发的 用秒就够了 位数填 32位够够的了 但是默认一定要规范 hm 42
    'snow_time_type' => env("SNOW_TIME_TYPE", 'microtime'),
    'snow_time_len' => env("SNOW_TIME_LEN", 42),
    // 序列数类型  rand 根据字节数随机  cache  根据默认缓存自增
    'snow_incr_type' => env("SNOW_INCR_TYPE", 'cache'),
    // 序列数 12字节 0-4095  10字节 0-1023
    'snow_incr_len' => env("SNOW_INCR_LEN", 12),

    // 美化输出的字符串  false => MP32-S91H97B9VPA true => MP32-0S91-H97B-9VPA
    'snow_beautify_code' => env("SNOW_BEAUTIFY_CODE", true),
    // 美化后输出的字符串分割长度 4 => MP32-0S91-H97B-9VPA 5 => MP32-00A3J-TWC2Y
    'snow_beautify_len' => env("SNOW_BEAUTIFY_LEN", 4),

    // int类型的id 转换成字符串类型用到的字典
    'snow_maps' => [
        'map32' => [
            'prefix' => 'MP32',
            // 默认 32位字典 10位数字和26位字母 去掉容易混淆的 O I L X 后剩下的 32位 你也可以自己把位置换了
            'map' => ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','J','K','M','N','P','Q','R','S','T','U','V','W','Y','Z'],
        ],
        // 自定义 下面可以随便定义自己的map mapsub就是自己随便定义的
        'mapsub' => [
            'prefix' => 'MPSU',
            // 自定map
            'map' => ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z'],
        ],

    ],
];
