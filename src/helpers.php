<?php

use Stevema\SnowFlake\SnowFlakeManage;
if(! function_exists('snowflake')){
    function snowflake():SnowFlakeManage{
        if(function_exists('app')) {
            return app('snowflake');
        }
        $config_file_path = realpath(__DIR__.'/../Config/config.php');
        $config = require($config_file_path);
        return new SnowFlakeManage($config);
    }
}
if(! function_exists('snowflake_create_id')){
    function snowflake_create_id(string $type='int'):float|int|string {
        $snow = app('snowflake');
        return $snow->createId($type);
    }
}
if(! function_exists('snowflake_encode')){
    function snowflake_encode(int $intId, string $mapName){
        $snow = app('snowflake');
        return $snow->encode($intId, $mapName);
    }
}
if(! function_exists('snowflake_decode')){
    function snowflake_decode(string $strId){
        $snow = app('snowflake');
        return $snow->decode($strId);
    }
}
